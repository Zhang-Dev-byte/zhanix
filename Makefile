C_SOURCES = $(wildcard src/kernel/*.cpp)

OBJ = loader/loader.o  src/kernel/kernel.o src/libc/printf.o src/drivers/port.o src/drivers/screen.o src/libc/clrscr.o src/libc/mem.o src/libc/itoa.o

GPPFLAGS = -m32 -ffreestanding -fno-exceptions -fno-rtti -nostdlib -nodefaultlibs -Isrc -Isrc/libc

%.o: %.asm
	nasm $< -felf -o $@

%.o: %.cpp
	g++ -c $< -o $@ ${GPPFLAGS}

Zhanix.bin: ${OBJ}
	ld -m elf_i386 ${OBJ} -T linker.ld -o $@

Zhanix.iso: Zhanix.bin
	mkdir iso
	mkdir iso/boot
	mkdir iso/boot/grub
	cp Zhanix.bin iso/boot/Zhanix.bin
	echo 'set timeout=0'                      > iso/boot/grub/grub.cfg
	echo 'set default=0'                     >> iso/boot/grub/grub.cfg
	echo ''                                  >> iso/boot/grub/grub.cfg
	echo 'menuentry "Zhanix" {' >> iso/boot/grub/grub.cfg
	echo '  multiboot /boot/Zhanix.bin'    >> iso/boot/grub/grub.cfg
	echo '  boot'                            >> iso/boot/grub/grub.cfg
	echo '}'                                 >> iso/boot/grub/grub.cfg
	grub-mkrescue --output=Zhanix.iso iso
	rm -rf iso

clean:
	rm Zhanix.bin ${OBJ} Zhanix.iso 

