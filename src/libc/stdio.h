#ifndef __LIBC_STDIO_H
#define __LIBC_STDIO_H

typedef char                     int8_t;
typedef unsigned char           uint8_t;
typedef short                   int16_t;
typedef unsigned short         uint16_t;
typedef int                     int32_t;
typedef unsigned int           uint32_t;
typedef long long int           int64_t;
typedef unsigned long long int uint64_t;

void printf(char*);
void memcpy(char* src, char* dest, int size);
void memset(uint8_t* dest, uint8_t src, uint32_t size);
void itoa(int n, char str[]);
#endif