#include "stdio.h"

void memcpy(char* src, char* dest, int size)
{
    int i;
    for (i = 0; i < size; i++) {
        *(dest + i) = *(src + i);
    }
}
void memset(uint8_t* dest, uint8_t src, uint32_t size)
{
    uint8_t *temp = (uint8_t *)dest;
    for ( ; size != 0; size--) *temp++ = src;
}